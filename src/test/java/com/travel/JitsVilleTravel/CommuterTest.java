package com.travel.JitsVilleTravel;

import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;

public class CommuterTest {

	private Commuter commuter;
	private String expectedName;
	private String actualName;
	private String expectedPassengerId;
	private String actualPassengerId;
	private double expectedStops;
	private double actualStops;
	private boolean expectedFrequentRider;
	private boolean actualFrequentRider;
	private boolean expectedRequestsNewsPaper;
	private boolean actualRequestsNewsPaper;
	
	@Before
	public void setUp() {
		commuter = new Commuter("Abhishek", "COMMUTE-axkfd123", 5, true, true);
		this.expectedName = "Abhishek";
		this.expectedPassengerId = "COMMUTE-axkfd123";
		this.expectedStops = 5;
		this.expectedRequestsNewsPaper = true;
		this.expectedFrequentRider = true;
	}
	
	@Test
	public void testGetPassengerName() {
		this.actualName = commuter.getPassengerName();
		assertEquals(this.expectedName, this.actualName);
	}
	
	@Test
	public void testGetPassengerId() {
		this.actualPassengerId = commuter.getPassengerId();
		assertEquals(this.expectedPassengerId, this.actualPassengerId);
	}
	
	@Test
	public void testGetNumberOfStops() {
		this.actualStops = commuter.getNumberOfStops();
		assertEquals(this.expectedStops, this.actualStops, 0.001);
	}
	
	@Test
	public void testRequestsNewsPaper() {
		this.actualRequestsNewsPaper = commuter.isRequestNewsPaper();
		assertEquals(this.expectedRequestsNewsPaper, this.actualRequestsNewsPaper);
	}
	
	@Test
	public void testFrequentRider() {
		this.actualFrequentRider = commuter.isFrequentRider();
		assertEquals(this.expectedFrequentRider, this.actualFrequentRider);
	}

}
