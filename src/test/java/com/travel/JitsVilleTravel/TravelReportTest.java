package com.travel.JitsVilleTravel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TravelReportTest {
	
	//Create an field variable for the TravelReport class
	private TravelReport report;
	private int expected;
	private int actual;
	private double expectedCost;
	private double actualCost;

	@Before
	public void setUp() {
		report = new TravelReport();
		report.addPassenger("vacationer", "Chammak Challo", 190, true);
		report.addPassenger("commuter", "Gandhari", 3, true,  true);
		report.addPassenger("vacationer", "Kavya Manda", 200, false);
		report.addPassenger("commuter", "Abhishek", 3, true, true);
		this.expected = 3;
		this.expectedCost = 197.70;
	}
	
	@Test
	public void testReport() {
		actual = report.getNewsPaperCount();
		assertEquals(expected, actual, 0.001);
		report.printReport();
	}
	
	@Test
	public void calculateCost() {
		this.actualCost = report.getTotalTripCost();
		assertEquals(this.expectedCost, this.actualCost, 0.001);
	}

}
