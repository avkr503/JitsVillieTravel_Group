package com.travel.JitsVilleTravel;

public class Commuter implements Passenger {
	private String passengerName;
	private String passengerId;
	private int numberOfStops;
	private boolean requestsNewsPaper;
	private boolean isFrequentRider;
	private double tripCost;
	
	public Commuter(String passengerName, String passengerId, int numberOfStops, boolean requestsNewsPaper, boolean isFrequentRider) {
		this.passengerId = passengerId;
		this.passengerName = passengerName;
		this.numberOfStops = numberOfStops;
		this.requestsNewsPaper = requestsNewsPaper;
		this.isFrequentRider = isFrequentRider;
		this.tripCost = 0;
		this.tripCost();
	}
	
	public Commuter() {
		// TODO Auto-generated constructor stub
	}

	public double tripCost() {
		double cost = 0.0;
		cost = this.getNumberOfStops() * 0.5;
		if(this.isFrequentRider()) {
			cost = cost - (0.1 * cost);
		}
		this.tripCost = cost;
		return cost;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}

	public int getNumberOfStops() {
		return numberOfStops;
	}

	public void setNumberOfStops(int numberOfStops) {
		this.numberOfStops = numberOfStops;
	}

	public void setRequestsNewsPaper(boolean requestsNewsPaper) {
		this.requestsNewsPaper = requestsNewsPaper;
	}

	public boolean isFrequentRider() {
		return isFrequentRider;
	}

	public void setFrequentRider(boolean isFrequentRider) {
		this.isFrequentRider = isFrequentRider;
	}

	public boolean isRequestNewsPaper() {
		// TODO Auto-generated method stub
		return this.requestsNewsPaper;
	}
}
