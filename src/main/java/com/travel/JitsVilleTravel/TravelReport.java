package com.travel.JitsVilleTravel;

import java.util.ArrayList;
import java.util.List;

public class TravelReport {
	private int newsPaperCount;
	PassengerFactory factory;
	Passenger passenger;
	private List<Passenger> passengers;
	private int vacationerMeals;
	
	public TravelReport() {
		this.newsPaperCount = 1;
		this.passengers = new ArrayList<Passenger>();
		this.vacationerMeals = 0;
	}
	
	public void addPassenger(String type, String passengerName, double numberOfStops, boolean isFrequentRider, boolean requestsNewsPaper) {
		factory = new PassengerFactory(type);
		
		passenger = factory.addCommuter(passengerName, numberOfStops, isFrequentRider, requestsNewsPaper);
		
		passengers.add(passenger);
		
	}
	
	public void addPassenger(String type, String passengerName, double numberOfMiles, boolean requestsNewsPaper) {
		factory = new PassengerFactory(type);
		
		passenger = factory.addVacationer(passengerName, numberOfMiles, requestsNewsPaper);
		
		passengers.add(passenger);
	}
	
	//Returns the total number of newspapers required
	public int getNewsPaperCount() {
		Newspaper newspaper = new Newspaper();
		newsPaperCount = newspaper.countNewsPapers(passengers);
		return newsPaperCount;
	}
	
	
	public void printReport() {
		System.out.println("Total newspapers:" + this.getNewsPaperCount());
		System.out.println("Total number of passengers : " +passengers.size());
		System.out.println("Total trip cost: " + this.getTotalTripCost());
	}
	
	public double getTotalTripCost() {
		TotalTripCost tripCost = new TotalTripCost();
		double totalCost = tripCost.calculateTotalTripCost(passengers);
		return totalCost;
	}

}
