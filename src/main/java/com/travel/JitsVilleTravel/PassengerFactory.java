package com.travel.JitsVilleTravel;

import java.util.UUID;

public class PassengerFactory {
	//private String passengerType;
	private Passenger passenger;
	
	public PassengerFactory(String passengerType) {
		if(passengerType.equalsIgnoreCase("vacationer")) {
			this.passenger = new Vacationer();
		}
		else if(passengerType.equalsIgnoreCase("commuter")){
			passenger = new Commuter();
		}
	}
	
	public Passenger addCommuter(String passengerName, double numberOfStops, boolean isFrequentRider, boolean requestsNewsPaper) {
		String passengerId = "COMMUTE" + UUID.randomUUID().toString();
		Commuter commuter = new Commuter();
		commuter.setPassengerId(passengerId);
		commuter.setPassengerName(passengerName);
		commuter.setNumberOfStops((int)numberOfStops);
		commuter.setFrequentRider(isFrequentRider);
		commuter.setRequestsNewsPaper(requestsNewsPaper);
		this.passenger = commuter;
		return passenger;
	}
	
	public Passenger addVacationer(String passengerName, double numberOfMiles, boolean requestsNewsPaper) {
		String passengerId = "VAC" + UUID.randomUUID().toString();
		Vacationer vacationer = new Vacationer();
		vacationer.setPassengerId(passengerId);
		vacationer.setPassengerName(passengerName);
		vacationer.setMiles(numberOfMiles);
		vacationer.setRequestNewsPaper(requestsNewsPaper);
		this.passenger = vacationer;
		return this.passenger;
	}
}
