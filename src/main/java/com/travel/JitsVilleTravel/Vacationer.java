package com.travel.JitsVilleTravel;

public class Vacationer implements Passenger{
	
	private String passengerName;
	private String passengerId;
	private boolean requestNewsPaper;
	private int numberOfMeals;
	private double miles;
	private double tripCost;
	
	public Vacationer(String passengerName, String passengerId, boolean requestNewsPaper, double miles) {
		this.passengerName = passengerName;
		this.passengerId = passengerId;
		this.requestNewsPaper = requestNewsPaper;
		this.miles = miles;
		this.tripCost = 0.0;
		this.tripCost();
	}

	public Vacationer() {
		
		// TODO Auto-generated constructor stub
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}

	public boolean isRequestNewsPaper() {
		return requestNewsPaper;
	}

	public void setRequestNewsPaper(boolean requestNewsPaper) {
		this.requestNewsPaper = requestNewsPaper;
	}

	public int getNumberOfMeals() {
		return numberOfMeals;
	}

	public double getMiles() {
		return miles;
	}

	public void setMiles(double miles) {
		this.miles = miles;
	}

	public double tripCost() {
		double cost = 0.0;
		if(this.getMiles() >=5 && this.getMiles() <= 4000) {
			cost = this.getMiles() * 0.5;
		
			this.tripCost = cost;
		}
		else
		{
			throw new IllegalArgumentException();
		}
		return cost;
	}
	
	public double getTripCost() {
		return tripCost;
	}

	public void setTripCost(double tripCost) {
		this.tripCost = tripCost;
	}

	public void setNumberOfMeals(int numberOfMeals) {
		this.numberOfMeals = numberOfMeals;
	}

	public void setNumberOfMeals() {
		int meals = 0;
		
		meals =(int) (miles+50) / 100;
		this.numberOfMeals = meals;
	}
}
