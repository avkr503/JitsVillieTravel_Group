/*
 *Purpose of this class is to calculate the total number of newspapers required
 */

package com.travel.JitsVilleTravel;

import java.util.List;

public class Newspaper {
	Passenger passenger;
	
	public Newspaper() {
		
	}
	
	public int countNewsPapers(List<Passenger> passengerList) {
		int total = 0;
		for(Passenger passenger: passengerList) {
			if(passenger.isRequestNewsPaper() == true) {
				total++;
			}
		}
		return total;
	}
}
