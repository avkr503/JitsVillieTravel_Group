package com.travel.JitsVilleTravel;

public interface Passenger {
	
	public double tripCost();
	public boolean isRequestNewsPaper();
	
}
