package com.travel.JitsVilleTravel;

import java.util.List;

public class TotalTripCost {
	
	public TotalTripCost() {
		
	}
	
	public double calculateTotalTripCost(List<Passenger> passengerList) {
		double totalTripFare = 0.0;
		
		for(Passenger passenger: passengerList) {
			totalTripFare += passenger.tripCost();
		}
		
		return totalTripFare;
	}
}
